# nau-ant

## 0.2.6

### Patch Changes

- 0

## 0.2.5

### Patch Changes

- fix release job

## 0.2.4

### Patch Changes

- 0

## 0.2.3

### Patch Changes

- -

## 0.2.2

### Patch Changes

- e24cc65: add release ci job

## 0.2.1

### Patch Changes

- 7302a43: edit tsconfig

## 0.2.0

### Minor Changes

- b9844fa: moved core package to root dir
