export function copyFileToTargetDir(fileName: string): Promise<void>;
export function createPackageJson(): Promise<void>;
