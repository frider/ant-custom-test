# @nau-ant/sandbox

## 0.2.7

### Patch Changes

- Updated dependencies
  - nau-ant@0.2.6

## 0.2.6

### Patch Changes

- Updated dependencies
  - nau-ant@0.2.5

## 0.2.5

### Patch Changes

- Updated dependencies
  - nau-ant@0.2.4

## 0.2.4

### Patch Changes

- Updated dependencies
  - nau-ant@0.2.3

## 0.2.3

### Patch Changes

- Updated dependencies [e24cc65]
  - nau-ant@0.2.2

## 0.2.2

### Patch Changes

- 7302a43: edit tsconfig
- Updated dependencies [7302a43]
  - nau-ant@0.2.1

## 0.2.1

### Patch Changes

- b9844fa: update core dependency
- 92fb6c0: test12345
- Updated dependencies [b9844fa]
  - nau-ant@0.2.0

## 0.2.0

### Minor Changes

- test minor change

### Patch Changes

- test patch

## 0.1.9

### Patch Changes

- Updated dependencies
  - @nau-ant/core@0.2.0

## 0.1.8

### Patch Changes

- Updated dependencies
  - @nau-ant/core@0.1.5

## 0.1.7

### Patch Changes

- Updated dependencies
  - @nau-ant/core@0.1.4

## 0.1.6

### Patch Changes

- refactor package

## 0.1.5

### Patch Changes

- fix readme sandbox

## 0.1.4

### Patch Changes

- 13a98fc: test1

## 0.1.3

### Patch Changes

- 37f1f85: test publishing
